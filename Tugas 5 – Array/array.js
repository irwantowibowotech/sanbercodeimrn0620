console.log("============================ Jawaban No 1 ============================");
function range(startNum = 0, finishNum = 0) {
	var arrayResult = []
	if(startNum == 0 || finishNum == 0) {
		return -1;
	} else {
		if(startNum > finishNum) {
			for(startNum; startNum >= finishNum; startNum--) {
				arrayResult.push(startNum);
			}
			return arrayResult;
		} else {
			for(startNum; startNum <= finishNum; startNum++) {
				arrayResult.push(startNum);
			}
			return arrayResult;
		}
	}
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54, 50));
console.log(range());
console.log();

console.log("============================ Jawaban No 2 ============================");
function rangeWithStep(startNum, finishNum, step) {
	var arrayResult2 = [];
	if(startNum < finishNum) {
		for(startNum; startNum <= finishNum; startNum += step) {
			arrayResult2.push(startNum);
		}
		return arrayResult2;
	} else {
		for(startNum; startNum >= finishNum; startNum -= step) {
			arrayResult2.push(startNum);
		}
		return arrayResult2;
	}
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));
console.log();

console.log("============================ Jawaban No 3 ============================");
function rangeStep(startNum = 0, finishNum = 0, step = 0) {
	var arrayStep = [];
	var total = 0;
	if(finishNum == 0) {
		return total = startNum;
	} else {
		if(step == 0) {
			if(startNum < finishNum) {
				for(startNum; startNum <= finishNum; startNum++) {
					arrayStep.push(startNum);
				}
			} else {
				for(startNum; startNum >= finishNum; startNum--) {
					arrayStep.push(startNum);
				}
			}
		} else {
			if(startNum < finishNum) {
				for(startNum; startNum <= finishNum; startNum += step) {
					arrayStep.push(startNum);
				}
			} else {
				for(startNum; startNum >= finishNum; startNum -= step) {
					arrayStep.push(startNum);
				}
			}
		}

		for(var i = 0; i < arrayStep.length; i++) {
			total += arrayStep[i];
		}
		return total;
	}
}

console.log(rangeStep(1,10));
console.log(rangeStep(5, 50, 2));
console.log(rangeStep(15, 10));
console.log(rangeStep(20, 10, 2));
console.log(rangeStep(1));
console.log(rangeStep());
console.log();

console.log("============================ Jawaban No 4 ============================");
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], 
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling() {
	for(var i = 0; i < input.length; i++) {
		console.log("Nomor ID: " + input[i][0]);
		console.log("Nama Lengkap: " + input[i][1]);
		console.log("TTL: " + input[i][2]);
		console.log("Hobi: " + input[i][3]);
		console.log();
	}
}

dataHandling();
console.log();

console.log("============================ Jawaban No 5 ============================");
function balikKata(word) {
	var wordReverse = "";
	for(var i = word.length - 1; i >= 0; i--) {
		wordReverse += word[i];
	}
	return wordReverse;
}
console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
console.log();

console.log("============================ Jawaban No 6 ============================");
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
function dataHandling2(parameter) {
	// buat salinan dari variable input
	var salinParam = parameter.slice();

	//replace elemen dengan splice
	salinParam.splice(1,1,"Roman Alamsyah Elsharawy");
	salinParam.splice(2,1,"Provinsi Bandar Lampung");
	salinParam.splice(4,0,"Pria");
	salinParam.splice(5,1,"SMA Internasional Metro");
	console.log(salinParam);

	//format date dan bulan
	month = salinParam[3].split("/");
	dateReverse = []

	var monthToString = '';
	switch(month[1]) {
		case '01':
			monthToString = "Januari";
		break;
		case '02':
			monthToString = "Febrauari";
		break;
		case '03':
			monthToString = "Maret";
		break;
		case '04':
			monthToString = "April";
		break;
		case '05':
			monthToString = "Mei";
		break;
		case '06':
			monthToString = "Juni";
		break;
		case '07':
			monthToString = "Juli";
		break;
		case '08':
			monthToString = "Agustus";
		break;
		case '09':
			monthToString = "September";
		break;
		case '10':
			monthToString = "Oktober";
		break;
		case '11':
			monthToString = "November";
		break;
		case '12':
			monthToString = "Desember";
		break;
		default:
	}
	console.log(monthToString);

	var monthDesc = month.sort(function(a, b) {return b - a});
	console.log(monthDesc);

	newFormatDate = salinParam[3].split("/").join("-");
	console.log(newFormatDate);
}

//menampilkan data element ke-1 dari input
console.log(input[1]);
