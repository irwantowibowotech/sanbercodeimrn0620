class Score {
	constructor(subject, point = [], email) {
		this.subject = subject
		this.point = point
		this.email = email
	}

	panggil() {
		console.log(this.point)
	}

	average() {
		let sum = 0
		let rata2 = 0
		for(let i = 0; i < this.point.length; i++) {
			sum += this.point[i]; 
		}
		rata2 = sum / this.point.length
		return rata2
	}
}

var hitungRata = new Score("Rata", [1,2,3,4,5], "aku@kamu.com")
console.log(hitungRata.rata());
