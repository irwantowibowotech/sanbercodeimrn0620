console.log("============================ Jawaban No 1 ============================");
function teriak() {
	return "Helo Sanbers!";
}

console.log(teriak());

console.log("============================ Jawaban No 2 ============================");
function kalikan(bil1, bil2) {
	return bil1 * bil2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("============================ Jawaban No 3 ============================");
function introduce(nama, umur, alamat, hobi) {
	return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi;
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

console.log("============================ Jawaban No 4 ============================");
