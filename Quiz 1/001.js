console.log("============================ Soal A Balik String ============================");
function balikString(word) {
	var wordReverse = "";
	for(var i = word.length - 1; i >= 0; i--) {
		wordReverse += word[i];
	}
	return wordReverse;
}
console.log(balikString("abcde"));
console.log(balikString("rusak"));
console.log(balikString("racecar"));
console.log(balikString("haji"));

console.log();

console.log("============================ Soal B Palindrome ============================");
function palindrome(kata) {
	kataBalik = '';
	for(var i = kata.length - 1; i >= 0; i--) {
		kataBalik += kata[i];
	}
	if(kataBalik == kata) {
		return true
	} else {
		return false
	}
}
console.log(palindrome("kasur rusak"));
console.log(palindrome("haji ijah"));
console.log(palindrome("nabasan"));
console.log(palindrome("nababan"));
console.log(palindrome("jakarta"));

console.log();

console.log("============================ Soal C Bandingkan Angka ============================");
function bandingkan(bilangan = null, pembanding = 0) {
	if(bilangan == null) {
		return -1;
	} else if(pembanding == 0) {
		return bilangan;
	} else {
		if((bilangan < 0) || (bilangan && pembanding < 0)) {
			return -1;
		} else {
			if(bilangan == pembanding) {
				return -1
			} else if(bilangan < pembanding) {
				return pembanding;
			} else {
				return pembanding;
			}
		}
	}
}

console.log(bandingkan(10, 15));
console.log(bandingkan(12, 12));
console.log(bandingkan(-1, 10));
console.log(bandingkan(112, 121));
console.log(bandingkan(1));
console.log(bandingkan());
console.log(bandingkan("15", "18"));
