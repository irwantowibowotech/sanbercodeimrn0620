console.log("============================ Soal A Ascending Ten ============================");
function AscendingTen(number = 0) {
	if(number == 0 || number == null) {
		return -1;
	} else {
		var arrayAscending = [];
		var ascendingNumber = number + 9;
		for(number; number <= ascendingNumber; number++) {
			arrayAscending.push(String(number));
		}
		return arrayAscending.join(' ');
	}
}

console.log(AscendingTen(11));
console.log(AscendingTen(21));
console.log(AscendingTen());

console.log();

console.log("============================ Soal B Descending Ten ============================");
function DescendingTen(angka = 0) {
	if(angka == 0 || angka == null) {
		return -1;
	} else {
		var arrayDescending = [];
		var descendingAngka = angka - 9;
		for(angka; angka >= descendingAngka; angka--) {
			arrayDescending.push(String(angka));
		}
		return arrayDescending.join(' ');
	}
}

console.log(DescendingTen(100));
console.log(DescendingTen(10));
console.log(DescendingTen());

console.log();

console.log("============================ Soal C Conditional Ascending Descending Ten ============================");
function ConditionalAscDesc(reference, check) {
	arrayAsc = [];
	arrayDesc = [];
	var ascendingReference = reference + 9;
	var descendingReference = reference - 9;

	if(check == null) {
		return -1;
	} else {
		if(check % 2 == 1) {
			for(reference; reference <= ascendingReference; reference++) {
				arrayAsc.push(String(reference));
			}
			return arrayAsc.join(' ');
		} else {
			for(reference; reference >= descendingReference; reference--) {
				arrayDesc.push(String(reference));
			}
			return arrayDesc.join(' ');
		}
	}
}
console.log(ConditionalAscDesc(20, 8));
console.log(ConditionalAscDesc(81, 1));
console.log(ConditionalAscDesc(31));
console.log(ConditionalAscDesc());

console.log();

console.log("============================ Soal D Papan Catur ============================");
function ularTangga() {
	var arr = []
	var result = ''

	for(var i = 100; i > 0; i--) {
	  arr.push(String(i))
	}

	var iter = 1
	for(i = arr.length - 1; i >= 0; i--) {
	  if(iter < 10) {
	    result += (arr[i] + ' ')
	    iter++
	  } else {
	    result += (arr[i] + '\n')
	    iter = 1
	  }
	}


	return result;
}

console.log(ularTangga());
