console.log("============= Mengubah fungsi menjadi fungsi arrow =============");
const golden = () => {
	return (
		console.log("this is golden!!")
	)
}

golden();

console.log();

console.log("============= Sederhanakan menjadi Object literal di ES6 =============");
const newFunction = (firstName, lastName) => {
	return {
		firstName,
		lastName,
		//Cara pertama saya
		// fullName: () => {
		// 	console.log(firstName + " " + lastName)
		// 	return
		// }

		//Cara kedua saya
		fullName: () => console.log(firstName + " " + lastName)
	}
} 
newFunction("William", "Imoh").fullName() 

console.log();

console.log("============= Destructuring =============");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation, spell} = newObject
console.log(firstName, lastName, destination, occupation)

console.log();

console.log("============= Array Spreading =============");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east];
console.log(combined)

console.log();

console.log("============= Template Literals =============");
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} incididunt ut labore et dolore magna aliqua. Ut enim  ad minim veniam`

console.log(before) 
