//LOOPING WHILE
console.log("======================== Looping While ===============================");
console.log("LOOPING PERTAMA");
// cara pertama ---> dengan mendefinisikan perulangan dari angka 0
/*
var kata = "I love coding";
var i = 0;
while(i < 20) {
	i += 2;
	console.log(i + " - " + kata);
}
*/

//cara kedua ---> dengan mendefinisikan perulangan awal dari angaka 2
console.log("LOOPING PERTAMA");
var kata = "I love coding";
var i = 2;
while(i <= 20) {
	console.log(i + " - " + kata);
	i += 2;
}
console.log("LOOPING KEDUA");
var kata2 = "I will become a mobile developer";
var j = 20;
while(j > 0) {
	console.log(j + " - " + kata2);
	j -= 2;
}

console.log()

//LOOPING FOR
console.log("======================== Looping For ===============================");
word = "";
for(var i = 1; i <= 20; i++) {
	if(i % 2 != 0) {
		if(i % 3 == 0) {
			word = "I Love Coding";
		} else {
			word = "Santai";
		}
	} else if(i % 2 == 0) {
		word = "Berkualitas";
	}
	console.log(i + " - " + word);
}
console.log();
console.log();

//MEMBUAT PERSEGI PANJANG
console.log("======================== Membuat Persegi panjang ===============================");
//Cara Pertama
/*
for(var i = 1; i <= 4; i++) {
	for(j = 0; i <= 8; i++ ) {
		console.log("#");
	}
}
*/

//Cara Kedua
var tag = ""
for(var x = 0; x <= 4; x++) {
	for(var y = 0; y < 8; y++) {
		tag += "#";
	}
	tag += "\n";
}
console.log(tag);

//MEMBUAT TANGGA
console.log("======================== Membuat Tangga ===============================");
//Cara Pertama
/*
for(var j = 1; j <= 7; j++) {
	console.log("#".repeat(j));
}
*/

//Cara kedua
var string = ""
for(var a = 0; a <= 7; a++) {
	for(var b = 0; b < a; b++) {
		string += "#";
	}
	string += "\n";
}
console.log(string);

//MEMBUAT PAPAN CATUR
console.log("======================== Membuat PAPAN CATUR ===============================");
var black = "#";
var white = " ";
var hasil = "";
var num = 8;
for(var i = 0; i < num; i++){ //buat vertikal
	for(var j = 0; j < num; j++) {//buat horizontal
		if(i % 2 != 0) { //Jika bilangan ganjil
			if(j % 2 == 0){ //Jika j bilangan genap
				hasil = hasil + black; // cetak pagar (#) --> black
			} else { //Jika j bilangan ganjil
				hasil = hasil + white; //cetak spasi saja ( ' ') --> white
			}
		} else { //Jika i adalah bilangan genap polanya berganti
			if(j % 2 == 0) { //Jika j adalah bilangan genap
				hasil = hasil + white; // cetak spasi (' ') --> white
			} else { // jika j ketemu bilangan ganjil
				hasil = hasil + black; // cetak pagar (#) --> black 
			}
		}
	}
	hasil = hasil + "\n"; // tambah enter/baris baru setiap selesai satu baris
	console.log(hasil); // cetak hasil setiap satu baris
	hasil = ""; // refresh wadah pola
}
