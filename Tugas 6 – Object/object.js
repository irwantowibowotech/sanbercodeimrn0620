console.log("============================ Soal 1 Array to Object ============================");
function arrayToObject(data) {
	var arrData = [];
	for(i = 0; i < data.length; i++) {
		const arrDataTemp = {}
		arrDataTemp['firstName'] = data[i][0];
		arrDataTemp['lastName'] = data[i][1];
		arrDataTemp['gender'] = data[i][2];
		var yearNow = new Date(Date.now());
		if(data[i][3] == null || data[i][3] > yearNow.getFullYear()) {
			arrDataTemp['age'] = "Invalid Birth Year";
		} else {
			arrDataTemp['age'] = yearNow.getFullYear() - data[i][3];
		}
		arrData.push(arrDataTemp);
	}

	for(var i = 0; i < arrData.length; i++) {
		console.log(i+1 + ". " + arrData[i].firstName + " " + arrData[i].lastName + "", arrData[i]);
		console.log()
	}
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(people);

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2);

console.log("============================ Soal 2 Shopping Time ============================");
var saleItem = [
	{brand: "Stacattu", harga: 1500000},
	{brand: "Uniklooh", harga: 175000},
	{brand: "H&N", harga: 250000},
	{brand: "Zoro", harga: 500000},
	{brand: "Casing HP", harga: 50000},
	{brand: "Coba HP", harga: 7000000},
];

//fungsi menurunkan harga dari yang paling besar ke paling kecil
function sortingHarga(a, b) {
	var comparison = 0;
	if(a.harga > b.harga) {
		comparison = -1;
	} else if(a.harga < b.harga) {
		comparison = 1;
	} else {
		comparison = 0;
	}
	return comparison;
}

saleItem.sort(sortingHarga)

function shoppingTime(memberId, money) {
	var sisaMoney = money;
	if(memberId == null) {
		console.log("Mohon maaf, toko X hanya berlaku untuk member saja");
		return;
	} 

	if(money < 50000) {
		console.log("Mohon maaf, uang tidak cukup");
		return;
	} 

	var cart = [];
	for(var x = 0; x < saleItem.length; x++) {
		if(sisaMoney >= saleItem[x].harga) {
			sisaMoney -= saleItem[x].harga
			cart.push(saleItem[x].brand);
		}
	}
	var arrDataPurchase = {};
	arrDataPurchase["memberId"] = memberId;
	arrDataPurchase["money"] = money;
	arrDataPurchase["listPurchased"] = cart;
	arrDataPurchase["changeMoney"] = sisaMoney;
	return arrDataPurchase;
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log();
console.log(shoppingTime('82Ku8Ma742', 170000));

console.log();

console.log("============================ Soal 3 Naik Angkot ============================");
function naikAngkot(arg) {
	var arrOrang = []
	var bayar = 0
	for(var i = 0; i < arg.length; i++) {
		//cara pertama menentukan nilai karakter
		rute = ["A", "B", "C", "D", "E", "F"];
		tempat1 = rute.indexOf(arg[i][1]);
		tempat2 = rute.indexOf(arg[i][2]);

		//cara kedua menentukan nilai karakter
		/*
		tempat1 = arg[i][1].charCodeAt();
		tempat2 = arg[i][2].charCodeAt();
		*/

		if(tempat2 > tempat1) {
			bayar = (tempat2 - tempat1) * 2000;
		} else {
			bayar = (tempat1 - tempat2) * 2000;
		}

		var dataOrang = {};
		dataOrang['penumpang'] = arg[i][0];
		dataOrang['Asal'] = arg[i][1];
		dataOrang['Tujuan'] = arg[i][2];
		dataOrang['Bayar'] = bayar;
		arrOrang.push(dataOrang);
	}
	return arrOrang;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));
