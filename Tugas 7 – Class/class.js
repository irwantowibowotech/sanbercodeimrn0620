console.log("============================ Soal 1 Animal Class ============================");
console.log("======== Release 0 ========");
class Animal {
	constructor(name, legs = 4, cold_blooded = false) {
		this._animalName = name;
		this._animalLegs = legs;
		this._coldBlooded = cold_blooded;
	}

	get animalName() {
		return this._animalName
	}
	get animalLegs() {
		return this._animalLegs;
	}
	get coldBlooded() {
		return this._coldBlooded;
	}

	set animalName(animal) {
		this._animalName = animal;
	}
}

var sheep = new Animal("Shaun");
console.log(sheep.animalName);
console.log(sheep.animalLegs);
console.log(sheep.coldBlooded);
console.log();

console.log("======== Release 1 ========");
class Ape extends Animal {
	constructor(name) {
		super(name);
	}

	yell() {
		console.log("Auooo");
	}
}

var sungkong = new Ape("Kera Sakti");
sungkong.yell();

class Frog extends Animal {
	constructor(name, legs) {
		super(name, legs);
	}

	jump() {
		console.log("hop hop");
	}
}	

var kodok = new Frog("buduk");
kodok.jump();

console.log();

console.log("============================ Soal 2 Function to Class ============================");
class Clock {
	constructor({template}, timer) {
		this.template = template
		this.timer = timer
	}

	render() {
		var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this.template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
	}

	stop() {
		this.render();
		this.timer = setInterval(this.render.bind(this), 1000)
	}

	start() {
		this.render();
		this.timer = setInterval(this.render.bind(this), 1000);
	}
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 
