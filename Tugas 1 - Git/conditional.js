// IF-ELSE
console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>If Else<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
var nama = "John";
var peran = "Penyihir";

if(nama) {
	if(peran == "") {
		console.log("Halo " + nama + ", Pilih peranmu untuk memulai game");
	} else if(peran == "Penyihir") {
		console.log("Selamat datang di Dunia Werewalf, " + nama);
		console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
	} else if(peran == "Guard") {
		console.log("Selamat datang di Dunia Werewalf, " + nama);
		console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
	} else {
		console.log("Selamat datang di Dunia Werewalf, " + nama);
		console.log("Halo " + peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
	}
} else {
	console.log("Nama harus diisi!");
}

console.log();
console.log();

//SWITCH-CASE
console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>If Else<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
var hari = 30;
var bulan = 4;
var tahun = 1945;

var month = '';

if(hari >= 1 && hari <= 31) {
	if(tahun >= 1900 && tahun <= 2020) {
		if(bulan == 2) {
			if(hari > 29) {
				console.log("Bulan Februari tanggal maksimal hanya 29");
			}
		} else {
			switch(bulan) {
				case 1: {month = 'Januari'; break;}
				case 2: {month = 'Februari'; break;}
				case 3: {month = 'Maret'; break;}
				case 4: {month = 'April'; break;}
				case 5: {month = 'Mei'; break;}
				case 6: {month = 'Juni'; break;}
				case 7: {month = 'Juli'; break;}
				case 8: {month = 'Agustus'; break;}
				case 9: {month = 'September'; break;}
				case 10: {month = 'Oktober'; break;}
				case 11: {month = 'November'; break;}
				case 12: {month = 'Desember'; break;}
				default: {console.log("Bulan yang dimaksud tidak ada")}
			}
			if((month == 'Januari' || month == 'Maret' || month == 'Mei' || month == 'Juli' || month == 'Agustus' || month == 'Oktober' || month == 'Desember') && (hari <= 31)) {
				console.log(hari + " " + month + " " + tahun);
			} else if((month == 'Februari' || month == 'April' || month == 'Juni' || month == 'September' || month == 'November') && (hari <= 30)) {
				console.log(hari + " " + month + " " + tahun);
			} else {
				console.log('Tanggal yang kamu masukkan salah');
			}
		}
	}
} else {
	console.log('Tanggal yang kamu maksud di luar jangkauan');
}
